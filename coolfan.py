'''

                            Online Python Compiler.
                Code, Compile, Run and Debug python program online.
Write your code in this editor and press "Run" button to execute it.

'''

import RPi.GPIO as GPIO
import time

def fanoff(boardpin):
    GPIO.setup(boardpin, GPIO.OUT, initial=GPIO.HIGH)
    
def fanon(boardpin):
    GPIO.setup(boardpin, GPIO.OUT, initial=GPIO.LOW)

def cpu_temp():
    with open("/sys/class/thermal/thermal_zone0/temp", 'r') as f:
        return float(f.read())/1000
    
def main():
    boardpin = 18
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    
    # close air fan first
    fanoff(boardpin)
    is_close = True
    while True:
        temp = cpu_temp()
        if is_close:
            if temp > 50.0:
                print (time.ctime(), temp)
                print('open air fan')
                fanon(boardpin)
                is_close = False
        else:
            if temp < 40.0:
                print (time.ctime(), temp)
                print ('close air fan')
                fanoff(boardpin)
                is_close = True

        time.sleep(5.0)
        print (time.ctime(), temp)

if __name__ == '__main__':
    main()
