# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

* Material
	
	1. raspberry pi 4B
	2. a fan which work with need 5VDC.
	3. a transistor S9012.
	
* Configuration		
	
	1. pin 18 connect to B of S9012.
	2. 5V connect to postive of fan
	3. GND of fan connect to E of S9012.
	4. GND of raspberry pi connect to C of S9012.

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
	Set up autorun when raspberry pi power on:
	1. open terminal
	2. enter "sudo nano /ect/rc.local"
	3. add "python3 /path/coolfan.py &" before last line "exit", remember that change path to your own path.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact